$(document).ready(function () {

  $("button").click(function () {
    var value = $("#inPuPut").val();
    if (value != "") {
        $("#list").append(
            "<li><i class='fas fa-trash-alt'></i><span>tâche : " + value + "</span></li>"
          );
          $("#inPuPut").val("");
    }
    
  });
  $( "#list" ).not("#lala").on( "click", "span", function( event ) {
    event.preventDefault();
    $(this).toggleClass("completed");
    });
});
$( "#list" ).on( "click", "i", function( event ) {
    event.preventDefault();
    $(this).parent().slideUp(500, function(){
        $(this).remove()
    });
});
